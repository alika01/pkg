package worker_pool

// ITask - interface for client to launch worker in pool
// it has two methods
// Do - main function to perform basic logic and returns bool to notify worker end of function (cuz each worker works in eternity loop) and error
// GetNumberGoroutines - number of goroutines, task should be set in
type ITask interface {
	Do() (bool, error)
	GetNumberGoroutines() int
}
