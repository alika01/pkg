package worker_pool

import (
	"log"
	"sync"
)

type IPool interface {
	Run()
}

type Pool struct {
	quit  chan struct{}
	tasks []ITask

	numWorker int

	quitOnce sync.Once
	wg       sync.WaitGroup
}

func NewPool(tasks []ITask) IPool {
	return &Pool{tasks: tasks}
}

func (p *Pool) Run() {
	p.addWorker()
	log.Printf("number of workers: %v\n have been launched", p.numWorker)
	p.wg.Wait()
	log.Printf("All workers successfully have been done\n")
}

func (p *Pool) addWorker() {
	for _, t := range p.tasks {
		for i := 1; i <= t.GetNumberGoroutines(); i++ {
			p.wg.Add(1)
			p.numWorker++
			go p.worker(t)
		}
	}
}

func (p *Pool) worker(t ITask) {
	defer func() {
		p.wg.Done()
		p.stop()
	}()

	for {
		select {
		case <-p.quit:
		default:
			isDone, err := t.Do()
			if err != nil {
				return
			}
			if isDone {
				return
			}
		}
	}
}

func (p *Pool) stop() {
	p.quitOnce.Do(func() {
		close(p.quit)
	})
}
