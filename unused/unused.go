package unused

import "fmt"

// upd unused
const (
	UnusedConst  = "unused"
	Unused2Const = "unused2"
)

// Unused - testing func
func Unused() {
	fmt.Println("test unused")
}

// Unused2 - testing func
func Unused2() {
	fmt.Println("test unused 2")
}
