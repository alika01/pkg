package cron

import "time"

type cron struct {
	every uint32
	jobs  []func()

	d      time.Duration
	ticker *time.Ticker
}

func NewCron() *cron {
	return &cron{}
}

func (c *cron) AddJobs(job ...func()) {
	c.jobs = append(c.jobs, job...)
}

func (c *cron) Every(num uint32) *cron {
	c.every = num
	return c
}

func (c *cron) Second() {
	c.d += time.Second
}

func (c *cron) Minute() {
	c.d += time.Minute
}

func (c *cron) Hour() {
	c.d += time.Hour
}

func (c *cron) RunSync() {
	c.setTicker()
	defer c.ticker.Stop()
	for {
		select {
		case <-c.ticker.C:
			c.runJobsSync()
		}
	}
}

func (c *cron) RunAsync() {
	c.setTicker()
	defer c.ticker.Stop()

	for {
		select {
		case <-c.ticker.C:
			c.runJobsAsync()
		}
	}
}

func (c *cron) runJobsSync() {
	for _, j := range c.jobs {
		j()
	}
}

func (c *cron) runJobsAsync() {
	for _, j := range c.jobs {
		go j()
	}
}

func (c *cron) setTicker() {
	c.ticker = time.NewTicker(time.Duration(c.every) * c.d)
}
